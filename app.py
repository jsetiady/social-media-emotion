from flask import Flask, request
from flask_restful import Resource, Api
from flask_cors import CORS
from json import dumps
import os
import scrapper.generate_statistics as stat

app = Flask(__name__)
CORS(app)
api = Api(app)

class Top_User(Resource):
    def get(self, period):
        result = stat.show_most_frequented_users(period)
        return result


class Main(Resource):
	def get(self):
		result = {'status': "success"}
		return result

class Important_Tweets(Resource):
    def get(self):
        return stat.show_most_important_tweets()

api.add_resource(Top_User, '/followers/top/<string:period>')
api.add_resource(Important_Tweets, '/tweets/important')
api.add_resource(Main, '/')

if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5000))
    app.run(debug=True, host='0.0.0.0', port=port)
	#app.run()
