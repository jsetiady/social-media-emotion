from gensim.models.keyedvectors import KeyedVectors
from preprocessing.Tokenizers import process_df, bigram_df
from preprocessing.Readers import tsv_to_dataframe
from keras.models import Sequential
from keras.layers import *
import numpy as np

x_train = np.empty([950,500,300])
x_test = np.empty([950,500,300])
y_train = np.empty([950,1,1])
y_test = np.empty([950,1,1])

model = KeyedVectors.load('./models/lstm.bin')
# to get a word vector for a word, simply model['word']
# vector dim = 300

train_df = tsv_to_dataframe('./datasets/facebook_part1.tsv')
test_df = tsv_to_dataframe('./datasets/facebook_part2.tsv')

train_sentences = bigram_df(process_df(train_df))
test_sentences = bigram_df(process_df(test_df))

def load():

    #loading training and testing x

    for i in range(950):
        for j in range(len(train_sentences["Text"][i])-2):
            try:
                #print train_sentences["Text"][i][j]+ " " + str(i)
                a = model[train_sentences["Text"][i][j]]
                x_train[i][j] = a
            except KeyError:
                x_train[i][j] = np.zeros((300))
                continue
        for k in range(len(train_sentences["Text"][i])+1,500):
            x_train[i][j] = np.zeros((300))
    
    for i1 in range(950):
        for j1 in range(len(test_sentences["Text"][i1])):
            try:
                x_test[i][j] = model[test_sentences["Text"][i][j]]
            except KeyError:
                x_test[i][j] = np.zeros((300))
                continue
        for k1 in range(len(test_sentences["Text"][i1])+1,500):
            x_test[i][j] = np.zeros((300))

    #loading training and testing y
    
    for i in range(950):
                y_train[i][0][0] = train_sentences["Sentiment"][i]
                        
    for i1 in range(950):
                y_test[i1][0][0] = test_sentences["Sentiment"][i]

def create_model():
    x_train1=x_train.transpose(0,2,1)
    y_train1=y_train.transpose(0,2,1)
    x_test1=x_test.transpose(0,2,1)
    y_test1=y_test.transpose(0,2,1)

    model = Sequential()
    model.add(Masking(mask_value=0., input_shape=(500, 300)))
    model.add(LSTM(300, input_shape=(500,300),return_sequences=True))
    #model.add(Permute((2,1)))
    model.add(AveragePooling1D(pool_size= 500,strides=None,padding='valid'))
    #model.add(GlobalAveragePooling1D())
    #print model.summary()
    #model.add(Permute((2,1)))
    model.add(Dense(1,activation='sigmoid'))
    print model.summary()
    model.compile(loss='mean_absolute_error',optimizer='adam',metrics=['accuracy'])
    model.fit(x_train,y_train,epochs=7,batch_size=200,verbose=1,validation_data=(x_test,y_test))
    #print model.summary()
    predict = model.predict(x_test)

load()
create_model()





