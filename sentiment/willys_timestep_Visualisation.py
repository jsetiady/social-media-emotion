'''This class is for visualising the timestep - frequency graph to help in picking the optimal timestep'''
from gensim.models.keyedvectors import KeyedVectors
from preprocessing.Readers import tsv_to_dataframe
from preprocessing.Tokenizers import process_df
from preprocessing.Tokenizers import bigram_df
from preprocessing.Tokenizers import df_to_tokenized_sentences
from preprocessing.Tokenizers import df_to_tokenized_2gram_sentences
from build_models.build_models import build_sentence_vector
from gensim.models import Word2Vec, Phrases
from gensim.models.keyedvectors import KeyedVectors
import gensim
import numpy as np
from sklearn.linear_model import LogisticRegression
from testing.Logger import graph_vs_datasize
from tests import log_all_classifier_tests
from sklearn.ensemble import RandomForestClassifier
import pandas as pd
import logging
import sys
import tensorflow as tf
import tflearn
from tflearn.data_utils import to_categorical, pad_sequences
from collections import defaultdict
import math
import matplotlib.pyplot as plt
train_file = './datasets/facebook_part1.tsv'
test_file  = './datasets/facebook_part2.tsv'

log = logging.getLogger()
log.setLevel(logging.DEBUG)

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter(
                    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)

log.addHandler(ch)

log.info("Tokenizing combined sentences")
train_df = tsv_to_dataframe(train_file)
test_df = tsv_to_dataframe(test_file)

train_sentences = bigram_df(process_df(train_df))
test_sentences = bigram_df(process_df(test_df))

#concatenate both train and test sentences for simplicity.
train_sentences= pd.concat([train_sentences,test_sentences],ignore_index=True)
log.info("Building Word2Vec")
# https://radimrehurek.com/gensim/models/word2vec.html
model = KeyedVectors.load('./models/lstm.bin')
# to get a word vector for a word, simply model['word']
# vector dim = 300


#--------------------------MAKE THE TENSORFLOW MODEL ---------------------------.




#----------Determine max timestep..---------------
'''We do this to ensure the optimal timestep tht represents
the majority of the data. I.e. doing timestep =500 words would be silly
as most comments are in fact only 19 words long.'''

# TODO: optimise timestep by using stdev, mean, tfidf, etc.---------------

#---------------------------YOUR CODE HERE ----------------------------------
m = len(train_sentences['Text'])
mxlen=0
num=0
idx=[]
freq=[]
lensfreq={}
lensfreq= defaultdict(lambda:0,lensfreq)

for i in xrange(len(train_sentences['Text'])):
    num+=len(train_sentences['Text'][i])
    lensfreq[len(train_sentences['Text'][i])]+=1

#clean up the data:
for key in lensfreq:
    idx.append(key)
    freq.append(lensfreq[key])

plt.plot(idx,freq,'ro')
plt.axis([1,275,0,120])
plt.title(' timestep - freq graph. x= timestep, y= frequency')
plt.xlabel('timestep')
plt.ylabel('frequency')
plt.grid(True)
plt.show()#I COMMENTED thiS Out.

avg_timesteps=num/m
print 'confirm timesteps. Press enter to confirm.'
print 'average= '+str(avg_timesteps)
print 'true avg:'+str(num/(m*1.0))

num_dimensions= 300
max_sequence=avg_timesteps
