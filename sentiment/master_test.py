from master import word_vectors, sentences, classifier
from testing.Logger import graph_vs_datasize

model_size=100

sens = sentences()
wv = word_vectors()

wv.load('keyedvectors','./models/lstm.bin')

clf = classifier(wv.model,300)

sens.default_process(unlabeled_files=['./datasets/tweets_large(18k).tsv',
                                       './datasets/fb_3(45k).tsv', 
                                       './datasets/fb_large(14k).tsv'],
                      labeled_files=['./datasets/facebook_part1.tsv',
                                      './datasets/facebook_part2.tsv'],
                     model=wv.model, model_size=300, sup_train_num=1200,
                     test_num=240, unsup_train_num=50000)


"""
TEST 1 
========
sens.import_tsvs(unlabeled_files=['./datasets/tweets_large(18k).tsv',
                                       './datasets/fb_3(45k).tsv', 
                                       './datasets/fb_large(14k).tsv'],
                      labeled_files=['./datasets/facebook_part1.tsv',
                                      './datasets/facebook_part2.tsv'])

sens.split_labeled(train_num=1000, test_num=900)

sens.make_unsupervised_train(50000)

wv.train('word2vec', sens.unsupervised_train["Text"], model_size)

sens.vectorize_to_array(wv.model, model_size)

sens.labels_to_array()

clf.maxent.fit(sens.train_array, sens.train_array_labels)
"""
graph_vs_datasize('master test', clf.maxent, 10, 'f1',
                    sens.train_array, sens.train_array_labels,
                    sens.test_array, sens.test_array_labels)
