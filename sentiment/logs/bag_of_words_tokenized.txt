File Train : ./datasets/facebook_part1.tsv
File Test : ./datasets/facebook_part2.tsv
Maximum Entropy 10-fold Accuracy: 0.66 (+/- 0.07)
Maximum Entropy Accuracy: 0.70
Maximum Entropy F1 score: 0.70
Maximum Entropy Recall: 0.70
Maximum Entropy Precision: 0.70
Maximum Entropy clasification report:
             precision    recall  f1-score   support

         -1       0.69      0.71      0.70       383
          0       0.72      0.75      0.73       488
          1       0.66      0.38      0.49        91

avg / total       0.70      0.70      0.70       962

Maximum Entropy confussion matrix:
272 108 3
106 367 15
19 37 35

Random Forest 10-fold Accuracy: 0.66 (+/- 0.09)
Random Forest Accuracy: 0.71
Random Forest F1 score: 0.71
Random Forest Recall: 0.71
Random Forest Precision: 0.71
Random Forest clasification report:
             precision    recall  f1-score   support

         -1       0.68      0.77      0.72       383
          0       0.76      0.71      0.73       488
          1       0.58      0.44      0.50        91

avg / total       0.71      0.71      0.71       962

Random Forest confussion matrix:
294 86 3
114 348 26
25 26 40

Naive Bayes 10-fold Accuracy: 0.53 (+/- 0.13)
Naive Bayes Accuracy: 0.57
Naive Bayes F1 score: 0.60
Naive Bayes Recall: 0.57
Naive Bayes Precision: 0.66
Naive Bayes clasification report:
             precision    recall  f1-score   support

         -1       0.72      0.55      0.62       383
          0       0.68      0.56      0.62       488
          1       0.25      0.71      0.37        91

avg / total       0.66      0.57      0.60       962

Naive Bayes confussion matrix:
211 117 55
68 275 145
14 12 65

SVM 10-fold Accuracy: 0.51 (+/- 0.06)
SVM Accuracy: 0.60
SVM F1 score: 0.53
SVM Recall: 0.60
SVM Precision: 0.59
SVM clasification report:
             precision    recall  f1-score   support

         -1       0.76      0.28      0.41       383
          0       0.57      0.95      0.71       488
          1       0.00      0.00      0.00        91

avg / total       0.59      0.60      0.53       962

SVM confussion matrix:
109 274 0
24 464 0
10 81 0

MLP 10-fold Accuracy: 0.65 (+/- 0.06)
MLP Accuracy: 0.71
MLP F1 score: 0.71
MLP Recall: 0.71
MLP Precision: 0.71
MLP clasification report:
             precision    recall  f1-score   support

         -1       0.68      0.74      0.71       383
          0       0.75      0.72      0.74       488
          1       0.57      0.48      0.52        91

avg / total       0.71      0.71      0.71       962

MLP confussion matrix:
285 92 6
110 351 27
24 23 44
