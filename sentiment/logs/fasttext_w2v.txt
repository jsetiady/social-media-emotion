File Train : ./datasets/facebook_part1.tsv
File Test : ./datasets/facebook_part2.tsv
Maximum Entropy 10-fold Accuracy: 0.54 (+/- 0.08)
Maximum Entropy Accuracy: 0.53
Maximum Entropy F1 score: 0.51
Maximum Entropy Recall: 0.53
Maximum Entropy Precision: 0.48
Maximum Entropy clasification report:
             precision    recall  f1-score   support

         -1       0.48      0.54      0.51       383
          0       0.57      0.62      0.60       488
          1       0.00      0.00      0.00        91

avg / total       0.48      0.53      0.51       962

Maximum Entropy confussion matrix:
208 175 0
183 303 2
40 51 0

Random Forest 10-fold Accuracy: 0.58 (+/- 0.08)
Random Forest Accuracy: 0.68
Random Forest F1 score: 0.66
Random Forest Recall: 0.68
Random Forest Precision: 0.67
Random Forest clasification report:
             precision    recall  f1-score   support

         -1       0.66      0.69      0.68       383
          0       0.69      0.75      0.72       488
          1       0.62      0.22      0.33        91

avg / total       0.67      0.68      0.66       962

Random Forest confussion matrix:
266 115 2
114 364 10
22 49 20

Naive Bayes 10-fold Accuracy: 0.48 (+/- 0.10)
Naive Bayes Accuracy: 0.47
Naive Bayes F1 score: 0.42
Naive Bayes Recall: 0.47
Naive Bayes Precision: 0.54
Naive Bayes clasification report:
             precision    recall  f1-score   support

         -1       0.44      0.87      0.59       383
          0       0.68      0.22      0.33       488
          1       0.27      0.13      0.18        91

avg / total       0.54      0.47      0.42       962

Naive Bayes confussion matrix:
334 40 9
357 108 23
67 12 12

SVM 10-fold Accuracy: 0.45 (+/- 0.01)
SVM Accuracy: 0.51
SVM F1 score: 0.34
SVM Recall: 0.51
SVM Precision: 0.26
SVM clasification report:
             precision    recall  f1-score   support

         -1       0.00      0.00      0.00       383
          0       0.51      1.00      0.67       488
          1       0.00      0.00      0.00        91

avg / total       0.26      0.51      0.34       962

SVM confussion matrix:
0 383 0
0 488 0
0 91 0

MLP 10-fold Accuracy: 0.52 (+/- 0.08)
MLP Accuracy: 0.54
MLP F1 score: 0.51
MLP Recall: 0.54
MLP Precision: 0.49
MLP clasification report:
             precision    recall  f1-score   support

         -1       0.50      0.53      0.52       383
          0       0.57      0.65      0.61       488
          1       0.00      0.00      0.00        91

avg / total       0.49      0.54      0.51       962

MLP confussion matrix:
204 179 0
170 318 0
34 57 0
