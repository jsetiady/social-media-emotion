import os.path
from sklearn.externals import joblib
import numpy
import logging
import sys
import pandas as pd
import numpy as np
import re
from nltk.corpus import stopwords
from bs4 import BeautifulSoup
from sklearn.feature_extraction.text import CountVectorizer
import preprocessor as p
from sklearn.metrics import precision_score, \
         recall_score, confusion_matrix, classification_report, \
         accuracy_score, f1_score
from sklearn.model_selection import cross_val_score
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn import svm
from sklearn.neural_network import MLPClassifier

file_train = '../datasets/facebook_part1.tsv'
file_test = '../datasets/facebook_part2.tsv'
file_save = '../models/bow.csv'
file_log  = '../logs/bow.txt'
file_data = '../datasets/data.tsv'

log = logging.getLogger()
log.setLevel(logging.DEBUG)

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter(
                    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)

log.addHandler(ch)

def log_classifier(f,classifier_name,classifier,x_train,y_train,x_test,y_test):

    fold = 10
    cv_score = cross_val_score(classifier, x_train, y_train, cv=fold)

    f.write('\n' + classifier_name + ' ' + str(fold) + "-fold Accuracy: %0.2f (+/- %0.2f)" % (cv_score.mean(), cv_score.std()*2))

    prediction = classifier.predict(x_test)

    f.write('\n' + classifier_name + ' Accuracy: %0.2f' % accuracy_score(y_test, prediction))
    f.write('\n' + classifier_name + ' F1 score: %0.2f' % f1_score(y_test, prediction, average='weighted'))
    f.write('\n' + classifier_name + ' Recall: %0.2f' % recall_score(y_test, prediction, average='weighted'))
    f.write('\n' + classifier_name + ' Precision: %0.2f' % precision_score(y_test, prediction, average='weighted'))
    f.write('\n' + classifier_name + ' clasification report:\n')
    f.write(classification_report(y_test, prediction))
    f.write('\n' + classifier_name + ' confussion matrix:\n')
    numpy.savetxt(f,confusion_matrix(y_test, prediction), fmt='%d')

def review_to_words(raw_review):
    tweet_text = BeautifulSoup(raw_review, "html.parser").get_text()
    
    p.set_options(p.OPT.URL, p.OPT.MENTION)
    clean_tweet_text = p.clean(tweet_text)

    letters_only = re.sub("[^a-zA-Z]", " ", clean_tweet_text)

    words = letters_only.lower().split()

    stops = set(stopwords.words("bahasa"))

    meaningful_words = [w for w in words if not w in stops]

    return " ".join(meaningful_words)

#Pre-proccessing
train = pd.read_csv(file_train, header=0,
                    delimiter="\t", quoting=3)

num_reviews= train["Text"].size

clean_train_reviews = []

train_num = num_reviews
for i in xrange( 0, train_num  ):
    clean_train_reviews.append( review_to_words(train["Text"][i]))
    if (i+1)%10 == 0:
        print "Review %d of %d\n" % (i+1,train_num)

#Creating Bag of Words
print "Creating the bag of words"

vectorizer = CountVectorizer(analyzer = "word",   
                             tokenizer = None,
                             preprocessor = None,
                             stop_words = None,
                             max_features = 500)

train_data_features = vectorizer.fit_transform(clean_train_reviews)

train_data_features = train_data_features.toarray()

vocab = vectorizer.get_feature_names()

dist = np.sum(train_data_features, axis=0)
for tag, count in zip(vocab, dist):
    print count, tag

#Training random forest
print "Training the random forest..."
from sklearn.ensemble import RandomForestClassifier

import os.path
from sklearn.externals import joblib
forest = RandomForestClassifier(n_estimators = 100) 
forest = forest.fit( train_data_features, train["Sentiment"][0:train_num])
print "Dumping model to file..."

#Cross-validation test
print "Starting cross-validation..."

from sklearn.model_selection import cross_val_score
scores = cross_val_score(forest, train_data_features, train["Sentiment"][0:train_num], cv=10)

print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))

print scores

#Pre-processing test data
test = pd.read_csv(file_test, header=0, delimiter="\t", 
                                      quoting=3 )

clean_test_reviews = [] 

num_test = len(test["Text"])
for i in xrange(0,num_test):
    if( (i+1) % 10 == 0 ):
        print "Review %d of %d\n" % (i+1, num_test)
    clean_review = review_to_words( test["Text"][i] )
    clean_test_reviews.append( clean_review )

#classying the test data

test_data_features = vectorizer.transform(clean_test_reviews)
test_data_features = test_data_features.toarray()

# output = pd.DataFrame( data={"ID":test["ID"][0:num_test], "Sentiment":result} )

log.info('Printing log')
log_file = open(file_log,'w')

# output.to_csv(log_file, index=False, quoting=3 )

test_num = test["Text"].size 

# print forest.score(test_data_features,test["Sentiment"][0:test_num])

naiveb = GaussianNB()
naiveb = naiveb.fit(train_data_features, train["Sentiment"][0:train_num])

svecm = svm.SVC()
svecm = svecm.fit(train_data_features, train["Sentiment"][0:train_num])

mlp = MLPClassifier(solver='adam', alpha=1e-5,
                    hidden_layer_sizes=(5, 2), random_state=1,
                    activation='tanh')

mlp.fit(train_data_features, train["Sentiment"][0:train_num])

maxent = LogisticRegression()
maxent.fit(train_data_features, train["Sentiment"][0:train_num])

log_file.write('File Train : ' + file_train)
log_file.write('\nFile Test : ' + file_test)
log_file.write('\nFile Data : ' + file_data + '\n')

log_classifier(log_file, 'Maximum Entropy', maxent, 
    train_data_features, train["Sentiment"][0:train_num], 
    test_data_features, test["Sentiment"][0:test_num])
log_classifier(log_file, 'Random Forest', forest,
    train_data_features, train["Sentiment"][0:train_num],
    test_data_features, test["Sentiment"][0:test_num])
log_classifier(log_file, 'Naive Bayes', naiveb,
    train_data_features, train["Sentiment"][0:train_num],
    test_data_features, test["Sentiment"][0:test_num])
log_classifier(log_file, 'SVM', svecm,
    train_data_features, train["Sentiment"][0:train_num],
    test_data_features, test["Sentiment"][0:test_num])
log_classifier(log_file, 'MLP', mlp,
    train_data_features, train["Sentiment"][0:train_num],
    test_data_features, test["Sentiment"][0:test_num])

log_file.close()
