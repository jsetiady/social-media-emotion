# file reader
import pandas as pd

# document filter
import re
from nltk.corpus import stopwords
from bs4 import BeautifulSoup
import preprocessor as p

# gensim modules
from gensim import utils
from gensim.models.doc2vec import TaggedDocument
from gensim.models import Doc2Vec, Phrases

# random shuffle
from random import shuffle

# numpy
import numpy

# classifier
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier

import logging
import sys

# import StemmerFactory class
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory

# create stemmer
factory = StemmerFactory()
stemmer = factory.create_stemmer()

# scoring classifier
from sklearn.metrics import precision_score, \
         recall_score, confusion_matrix, classification_report, \
         accuracy_score, f1_score
from sklearn.model_selection import cross_val_score

# location file definition
file_train = '../datasets/facebook_part1.tsv'
file_test = '../datasets/facebook_part2.tsv'
file_data = '../datasets/data.tsv'
file_save = '../models/2g_d2v.vec'
file_log = '../logs/d2v_bin.txt'

log = logging.getLogger()
log.setLevel(logging.DEBUG)

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter(
                    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)

log.addHandler(ch)

def review_to_words(raw_review):

    log.info("Pre-procesing sentences")
    tweet_text = BeautifulSoup(raw_review, "html.parser").get_text()
    p.set_options(p.OPT.URL, p.OPT.MENTION)
    clean_tweet_text = p.clean(tweet_text)
    letters_only = re.sub("[^a-zA-Z]", " ", clean_tweet_text)
    words = utils.to_unicode(letters_only.lower()).split()

    stops = set(stopwords.words("bahasa"))
    meaningful_words = [w for w in words if not w in stops]
    return meaningful_words

def sentiment_to_tag(sentiment, (pos,neg,net)):
    if sentiment == 1:
        return ("POS_%d" % pos, (pos+1,neg,net))
    elif sentiment  == -1:
        return ("NEG_%d" % neg, (pos,neg+1,net))
    elif sentiment == 0: 
        return ("NET_%d" % net, (pos,neg,net+1))

def sentence_to_bigram(phrases, sentence):
    unigram = review_to_words(sentence)

    for word in phrases[unigram]:
        if word not in unigram:
            unigram.append(word) 
    
    return unigram

def log_classifier(f,classifier_name,classifier,x_train,y_train,x_test,y_test):

    fold = 10
    cv_score = cross_val_score(classifier, x_train, y_train, cv=fold)

    f.write('\n' + classifier_name + ' ' + str(fold) + "-fold Accuracy: %0.2f (+/- %0.2f)" % (cv_score.mean(), cv_score.std()*2))
    
    prediction = classifier.predict(x_test)

    f.write('\n' + classifier_name + ' Accuracy: %0.2f' % accuracy_score(y_test, prediction))
    f.write('\n' + classifier_name + ' F1 score: %0.2f' % f1_score(y_test, prediction, average='weighted'))
    f.write('\n' + classifier_name + ' Recall: %0.2f' % recall_score(y_test, prediction, average='weighted'))
    f.write('\n' + classifier_name + ' Precision: %0.2f' % precision_score(y_test, prediction, average='weighted'))
    f.write('\n' + classifier_name + ' clasification report:\n') 
    f.write(classification_report(y_test, prediction))
    f.write('\n' + classifier_name + ' confussion matrix:\n')
    numpy.savetxt(f,confusion_matrix(y_test, prediction), fmt='%d')

class TaggedLineSentenceTSV(object):
    def __init__(self, source_train, source_test, source_data):

        self.train = pd.read_csv(source_train, header=0,
                                delimiter="\t", quoting=3)
        self.test = pd.read_csv(source_test, header=0,
                                delimiter="\t", quoting=3)
        self.data = pd.read_csv(source_data, header=0,
                                delimiter="\t", quoting=3)
        
        self.train_num = self.train["Text"].size
        self.test_num = self.test["Text"].size
        self.data_num = self.data["Text"].size
        
        aggregate = self.train["Text"].append(self.test["Text"].append(self.data["Text"]))

        self.phrases = Phrases([review_to_words(doc) for doc in aggregate],
                               min_count=1, 
                               threshold=2)

        self.train_pos_num = 0
        self.train_neg_num = 0
        self.train_net_num = 0
        for i in xrange(0, self.train_num):
            if self.train["Sentiment"][i] == 1:
                self.train_pos_num += 1
            elif self.train["Sentiment"][i] == -1:
                self.train_neg_num += 1
            elif self.train["Sentiment"][i] == 0:
                self.train_net_num += 1

        self.test_pos_num = 0
        self.test_neg_num = 0
        self.test_net_num = 0
        for i in xrange(0, self.test_num):
            if self.test["Sentiment"][i] == 1:
                self.test_pos_num += 1
            elif self.test["Sentiment"][i] == -1:
                self.test_neg_num += 1
            elif self.test["Sentiment"][i] == 0:
                self.test_net_num += 1

    def __iter__(self):

        pos = 0; neg = 0; net = 0
        for i in xrange(0 , self.train_num):
            tag, (pos,neg,net) = sentiment_to_tag(self.train["Sentiment"][i],
                                                  (pos,neg,net))

            list_train = sentence_to_bigram(self.phrases, self.train["Text"][i])

            yield TaggedDocument(list_train, ["TRAIN_" + tag])

        pos = 0; neg = 0; net = 0
        for i in xrange(0 , self.test_num):
            tag, (pos,neg,net) = sentiment_to_tag(self.test["Sentiment"][i],
                                                  (pos,neg,net))
            
            list_test = sentence_to_bigram(self.phrases, self.test["Text"][i])

            yield (TaggedDocument(list_test, ["TEST_" + tag]))


        for i in xrange(0, self.data_num ):
            list_data = sentence_to_bigram(self.phrases, self.data["Text"][i])

            yield (TaggedDocument(list_data, ["DATA_%d" % i]))

    def to_array(self):
        self.sentences = []

        pos = 0; neg = 0; net = 0
        for i in xrange(0 , self.train_num):
            tag, (pos,neg,net) = sentiment_to_tag(self.train["Sentiment"][i],
                                                  (pos,neg,net))
            
            list_train = (sentence_to_bigram(self.phrases, self.train["Text"][i]))

            self.sentences.append(TaggedDocument(list_train, 
                                                 ["TRAIN_" + tag]))
        pos = 0; neg = 0; net = 0
        for i in xrange(0 , self.test_num):
            tag, (pos,neg,net) = sentiment_to_tag(self.test["Sentiment"][i],
                                                  (pos,neg,net))

            list_test = (sentence_to_bigram(self.phrases, self.test["Text"][i]))

            self.sentences.append(TaggedDocument(list_test, 
                                                ["TEST_" + tag]))

        for i in xrange(0, self.data_num):
            list_data = (sentence_to_bigram(self.phrases, self.data["Text"][i]))

            self.sentences.append(TaggedDocument(list_data,
                                                 ["DATA_%d" % i]))

        return self.sentences

    def sentences_perm(self):
        shuffle(self.sentences)
        return self.sentences

log.info('Loading document')
sentences = TaggedLineSentenceTSV(file_train,  
                                  file_test,   
				                  file_data) 

log.info('D2V')
model_size = 100
model = Doc2Vec(min_count=1, window=10, size=model_size, 
                sample=1e-4, negative=5, workers=7,
                )
model.build_vocab(sentences.to_array())

import os.path
if not os.path.isfile(file_save):
    log.info('Epoch')
    for epoch in range(10):
	    log.info('EPOCH: {}'.format(epoch))
	    model.train(sentences.sentences_perm(),
                    total_examples=model.corpus_count,
                    epochs=model.iter)

    log.info('Model Save')
    model.save(file_save)
else:
    log.info('Model Load')
    model = Doc2Vec.load(file_save)

log.info('Sentiment')
train_arrays = numpy.zeros((sentences.train_num, model_size))
train_labels = numpy.zeros(sentences.train_num)

for i in range(sentences.train_pos_num):
    prefix_train_pos = 'TRAIN_POS_' + str(i)
    train_arrays[i] = model.docvecs[prefix_train_pos]
    train_labels[i] = 1

for i in range(sentences.train_neg_num):
    prefix_train_neg = 'TRAIN_NEG_' + str(i)
    train_arrays[sentences.train_pos_num + i] = model.docvecs[prefix_train_neg]
    train_labels[sentences.train_pos_num + i] = -1

for i in range(sentences.train_net_num):
    prefix_train_net = 'TRAIN_NET_' + str(i)
    train_arrays[sentences.train_pos_num 
                 + sentences.train_neg_num + i] = model.docvecs[prefix_train_net]
    train_labels[sentences.train_pos_num 
                 + sentences.train_neg_num + i] = 0

test_arrays = numpy.zeros((sentences.test_num, model_size))
test_labels = numpy.zeros(sentences.test_num)

for i in range(sentences.test_pos_num):
    prefix_test_pos = 'TEST_POS_' + str(i)
    test_arrays[i] = model.docvecs[prefix_test_pos]
    test_labels[i] = 1

for i in range(sentences.test_neg_num):
    prefix_test_neg = 'TEST_NEG_' + str(i)
    test_arrays[sentences.test_pos_num + i] = model.docvecs[prefix_test_neg]
    test_labels[sentences.test_pos_num + i] = -1

for i in range(sentences.test_net_num):
    prefix_test_net = 'TEST_NET_' + str(i)
    test_arrays[sentences.test_pos_num 
                + sentences.test_neg_num + i] = model.docvecs[prefix_test_net]
    test_labels[sentences.test_pos_num 
                + sentences.test_neg_num + i] = 0

log.info('Fitting')
maxent = LogisticRegression()
maxent.fit(train_arrays, train_labels)

forest = RandomForestClassifier(n_estimators=100)
forest = forest.fit(train_arrays, train_labels)

from sklearn.naive_bayes import GaussianNB
naiveb = GaussianNB()
naiveb = naiveb.fit(train_arrays, train_labels)

from sklearn import svm
svecm = svm.SVC()
svecm = svecm.fit(train_arrays, train_labels)

from sklearn.neural_network import MLPClassifier
mlp = MLPClassifier(solver='adam', alpha=1e-5,
                    hidden_layer_sizes=(5, 2), random_state=1,
                    activation='tanh')
mlp.fit(train_arrays, train_labels)

log.info('Printing log')
log_file = open(file_log,'w')

log_file.write('File Train : ' + file_train)
log_file.write('\nFile Test : ' + file_test)
log_file.write('\nFile Data : ' + file_data + '\n')

log_classifier(log_file, 'Maximum Entropy', maxent, train_arrays, train_labels, test_arrays, test_labels)
log_classifier(log_file, 'Random Forest', forest, train_arrays, train_labels, test_arrays, test_labels)
log_classifier(log_file, 'Naive Bayes', naiveb, train_arrays, train_labels, test_arrays, test_labels)
log_classifier(log_file, 'SVM', svecm, train_arrays, train_labels, test_arrays, test_labels)
log_classifier(log_file, 'MLP', mlp, train_arrays, train_labels, test_arrays, test_labels)

log_file.close()
