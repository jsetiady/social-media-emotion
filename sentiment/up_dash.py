from sklearn.externals import joblib
from sentiment.preprocessing.Readers import tsv_to_dataframe
from sentiment.preprocessing.Tokenizers import process_df, bigram_df
from build_models.build_models import build_sentence_vector
from gensim.models.keyedvectors import KeyedVectors
import pandas as pd
import numpy as np

def up_dash(in_file, out_file, clf_file = './models/lstmbin_maxent.pkl',
            model_file = './models/lstm.bin', model_size=300):
    model = KeyedVectors.load('./models/lstm.bin')
    print "Model loaded"
    clf = joblib.load('./models/lstmbin_maxent.pkl')
    print "Clf loaded"
    df = tsv_to_dataframe(in_file)
    print "Begin process df"
    proc_df = bigram_df(process_df(df))
    x_num = len(proc_df)
    dicts = []
    for i in xrange(0,x_num):
        x = build_sentence_vector(model, proc_df["Text"][i], model_size)
        y = int(clf.predict(x))
        dict = {"ID" : proc_df["ID"][i], "Sentiment" : y, "Speech_Type": 9}
        dicts.append(dict)
    out_df = pd.DataFrame(dicts)
    out_df.to_csv(out_file, sep = '\t')

up_dash('./datasets/facebook_part1.tsv','./datasets/up_dash.tsv')
