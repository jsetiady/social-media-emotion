import numpy as np
import pandas as pd

from crowdcrafting import labeled_only_df
from preprocessing.Readers import tsv_to_df, tsvs_to_df, shuffle
from preprocessing.Tokenizers import bigram_df, process_df, process_sentence
from build_models.build_models import vectorize_sentences, build_sentence_vector

# word vector models
from gensim.models import Word2Vec
from gensim.models.wrappers import FastText
from gensim.models.keyedvectors import KeyedVectors

#classifiers
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression

# graph vs datasize
import plotly
import plotly.plotly as py
import plotly.graph_objs as go
plotly.tools.set_credentials_file(username='rheza', api_key='6RMGKMc8L2StK1xUCEPv')

class word_vectors:
    # def __init__(self):
    def train(self, model_type, train_sentences, model_size):
        if model_type == 'word2vec':
            self.model = Word2Vec(train_sentences,
                                       size=model_size,
                                       workers=2,
                                       iter=5,
                                       window=5,
                                       sg=1,
                                       hs=1,
                                       min_count=5)
        elif model_type == 'fasttext':
            FastText(sentences=train_sentences, size=model_size)
    
    def load(self, model_type, model_file):
        # use wv with w2v to access word vector instances
        if model_type=='word2vec':
            self.model = Word2Vec.load(model_file)
        elif model_type=='keyedvectors':
            self.model = KeyedVectors.load(model_file)
        elif model_type=='fasttext':
            self.model = FastText.load_binary_data(model_file)
        else:
            print "Please input a valid model_type"
        
    def save(self, model_file):
        # if isinstance(self.model, Word2Vec):
        self.model.save(model_file)

class sentences:
    def import_tsvs(self, unlabeled_files=None, labeled_files=None):
        if unlabeled_files:
            self.unlabeled = tsvs_to_df(unlabeled_files)
        if labeled_files:
            self.labeled = tsvs_to_df(labeled_files)
    
    def tokenize(self, bigram=False, remove_punctuation=True, fix_typos=False):
        self.unlabeled = process_df(self.unlabeled, remove_punctuation, 
                                    fix_typos).reset_index()
        self.labeled = process_df(labeled_only_df(self.labeled), 
                                  remove_punctuation, fix_typos).reset_index()
        if bigram==True:
            self.unlabeled = bigram_df(self.unlabeled)
            self.labeled = bigram_df(self.labeled)

    def split_labeled(self, train_num, test_num):
        self.labeled = shuffle(self.labeled)
        self.labeled_train = self.labeled.head(train_num).reset_index()
        self.labeled_test = self.labeled.tail(test_num).reset_index()

    def make_unsupervised_train(self, train_num):
        all_train_sentences = pd.concat([self.unlabeled, self.labeled_train], 
                                        ignore_index=True)
        all_train_sentences = shuffle(all_train_sentences)
        self.unsupervised_train = all_train_sentences.head(train_num)

    def vectorize_to_array(self, model, model_size):
        self.train_array = vectorize_sentences(self.labeled_train["Text"],
                                               model, model_size)
        self.test_array = vectorize_sentences(self.labeled_test["Text"],
                                              model, model_size)

    def labels_to_array(self):
        self.train_array_labels = self.labeled_train["Sentiment"].as_matrix()
        self.test_array_labels = self.labeled_test["Sentiment"].as_matrix()

    def default_process(self, unlabeled_files, labeled_files, 
                        model, model_size, sup_train_num, 
                        test_num, unsup_train_num):
        self.import_tsvs(unlabeled_files, labeled_files)
        self.tokenize(bigram=True)
        self.split_labeled(sup_train_num, test_num)
        self.make_unsupervised_train(unsup_train_num)
        self.vectorize_to_array(model, model_size)
        self.labels_to_array()

class classifier:
    def __init__(self, wv_model, wv_model_size):
        self.maxent = LogisticRegression()
        self.forest = RandomForestClassifier()
        self.mlp = MLPClassifier()
        self.wvm = wv_model
        self.wvm_size = wv_model_size

    def load(self, clf_file):
        clf = joblib.load(clf_file)
        if isinstance(clf, LogisticRegression):
            self.maxent = clf
        elif isinstance(clf, RandomForestClassifier):
            self.forest = clf
        elif isinstance(clf, MLPClassifier):
            self.mlp = clf

    def classify_df(self, df):
        df = bigram_df(process_df(df))
        dicts = []
        for i in xrange(0, len(df)):
            x = build_sentence_vector(model, df["Text"][i], model_size)
            y = int(self.maxent.predict(x))
            dict = {"ID" : df["ID"][i], "Sentiment" : y}
            dicts.append(dict)
        return pd.DataFrame(dicts)

    def classify_tsv(self, in_file, out_file, model, clf):
        in_df = tsv_to_df(in_file)
        out_df = classify_df(in_df)
        out_df.to_csv(out_file, sep="\t")

    def classify_string(self, string, remove_punctuation=True, 
                        fix_typos=False):
        tokenized_sentence = process_sentence(string)
        vector = build_sentence_vector(self.wvm, tokenized_sentence, 
                                       self.wvm_size)
        return self.maxent.predict(vector)

