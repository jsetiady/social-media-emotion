#from gensim.models.keyedvectors import KeyedVectors
#from preprocessing.Readers import tsv_to_dataframe
from Readers import tsv_to_dataframe
from Tokenizers import process_df
from Tokenizers import bigram_df
from Tokenizers import df_to_tokenized_sentences
from Tokenizers import df_to_tokenized_2gram_sentences
from typofix import typodistance, spell

#from build_models.build_models import build_sentence_vector #not needed.
#from gensim.models import Word2Vec, Phrases
#from gensim.models.keyedvectors import KeyedVectors
#import gensim
import numpy as np
#from sklearn.linear_model import LogisticRegression
#from testing.Logger import graph_vs_datasize
#from tests import log_all_classifier_tests
#from sklearn.ensemble import RandomForestClassifier
import pandas as pd
import logging
import sys
#import tensorflow as tf
#import tflearn

from collections import Counter
import math

train_file = '../datasets/facebook_part1.tsv'
test_file  = '../datasets/facebook_part2.tsv'

log = logging.getLogger()
log.setLevel(logging.DEBUG)

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter(
                    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)

log.addHandler(ch)

log.info("Tokenizing combined sentences")
train_df = tsv_to_dataframe(train_file)
test_df = tsv_to_dataframe(test_file)

train_sentences = bigram_df(process_df(train_df))
test_sentences = bigram_df(process_df(test_df))

#concatenate both train and test sentences for simplicity.
train_sentences= pd.concat([train_sentences,test_sentences],ignore_index=True)

#------------------------------WORDSET VISUALISATIONS --------------------------
wordset=Counter()


for i in xrange(len(train_sentences['Text'])):
    for word in train_sentences['Text'][i]:
        wordset[word]+=1


def frequency_of(word):
    return wordset[word]

'''
We found out that the commonly occuring mispellings are:

'tidak': change to gak.
    'ga' = 295
    'gak'=250
    'gk'=104

'kalau': change to kalau.
    'klo'=141
    'kalo'=126

'''
