import Util.mongodb as database
import pprint, tweepy
from datetime import datetime, timedelta
from bson.json_util import dumps
import Util.twitter as util 
import sys
"""
A dashboard that contains:

1. Most frequented users - which users mention us the most often each week. Display this as a leaderboard.
2. Most important tweets from our timelines. Determined by which tweets had the most retweets or visibility (aka seen by the most followers)
3. New followers and followers who have unfollowed us each day
4. Current emotion. Define a few categories for emotions. Define how to classify a particular tweet into that category.

There is a lot of reference material available, but I would like to start with some text mining first. At the end, this project should present us with a dashboard.


1. Most frequented users
2. Most important tweets from our timelines
3. New Followers and Followers who have unfollowed us
4. Current emotion.
"""


def get_most_frequented_users(first_day, last_day):
    # Most frequented users - which users mention us the most often each week. Display this as a leaderboard.
    pipeline = [
        { "$match": {
            "created_time": {
                "$gte": first_day,
                "$lt": last_day
            }
        }
        },
        {"$group" :
         {"_id":
          {
              "username":"$username",
          }, 
          "count":{"$sum":1}}
         },
        {
            "$sort":
            {"count":-1}
        },
        {
            "$limit" : 30
        }
    ]
    result = db.tweets.aggregate(pipeline)
    json_result = []
    for r in result:
        user = {}
        username = r["_id"]["username"]
        user_data = db.followers.find_one({"screen_name" : username})
        if not user_data:
            auth = util.auth()
            api = tweepy.API(auth)
            not_inserted = True
            while not_inserted:
                try:
                    u = api.get_user(screen_name = username)
                    user_data = {
                        "id" : str(u.id),
                        "name" : u.name,
                        "screen_name" : u.screen_name,
                        "profile_image_url" : u.profile_image_url
                    }
                    db.followers.insert_one(user_data)
                    print "inserted"
                    not_inserted = False
                except Exception as e:
                    if not e[0][0]["code"] == 50:
                        print e
                        auth = util.auth()
                        api = tweepy.API(auth)
                    else:
                        not_inserted = False
        if user_data:
            json = {
                "count" : r["count"],
                "screen_name" : username,
                "id" : user_data["id"],
                "profile_image_url" : user_data["profile_image_url"]
            }
            json_result.append(json)
        print len(json_result)
    return json_result


def show_most_frequented_users(day):
    dt = datetime.strptime(day, "%Y-%m-%d")
    first_day = dt - timedelta(days = dt.weekday())
    last_day = first_day + timedelta(days = 6)
    
    data = get_most_frequented_users(first_day.strftime("%Y-%m-%d"), last_day.strftime("%Y-%m-%d"))

    return list(data)

def get_most_important_tweets():
    #Most Influential Tweets - tweets with the highest sum of favorites and retweets
    pipeline = [
        {"$project" : {
            'id' : '$id',
            'username' : '$username',
            'text_original' : '$text_original',
            'influence_score' : {
                "$sum" : [
                    '$retweet_count', '$favorite_count'
                ]
            },
        }
        },
        {
            "$sort" : 
            {"influence_score" : -1}
        },
        { "$limit" : 10 }
    ]
    result = db.tweets.aggregate(pipeline)

    json_data = []
    for r in result:
        json = {
            "id" : r["id"],
            "username" : r["username"].encode("latin1"),
            "influence_score" : r["influence_score"]
        }
        json_data.append(json)
    return json_data

def show_most_important_tweets():
    data = get_most_important_tweets()

    return data



db = database.connect()

# Get first day and last day
day = "2017-07-09"
#show_most_frequented_users(day)
day = "2017-07-16"
#show_most_frequented_users(day)
day = "2017-07-17"
#show_most_frequented_users(day)

