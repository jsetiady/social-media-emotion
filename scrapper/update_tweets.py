import tweepy, sys, timeit, datetime
import Util.twitter as util
import Util.mongodb as database
from time import gmtime, strftime

auth = util.auth()
api = tweepy.API(auth)
db = database.connect()
tweets = db.tweets
tweets_data = db.tweets.find().sort([("created_at", -1)]).limit(6000)


# Print all the first cell of all the rows
k = 1
start = timeit.default_timer()
for tweet in tweets_data:
    # Get tweet by id
    tweet_id = tweet["id"]
    try:
    	t = api.get_status(tweet_id)
    except Exception:
    	continue

    followers_count = t.user.followers_count
    retweet_count = t.retweet_count
    favorite_count = t.favorite_count

    count = tweets.update_one(
        {"id" : tweet_id},
        { "$set" : {
            "last_update" : str(datetime.datetime.utcnow()),
            "favorite_count" : str(favorite_count),
            "retweet_count" : str(retweet_count),
            "followers_count" : str(followers_count)
        }}, upsert = False
    ).matched_count
    
    if count == 0:
        print "Update error"
    #print "Update-" + str(k) + ": "+ str(tweet_id)

stop = timeit.default_timer()
print stop - start

